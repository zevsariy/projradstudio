//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStartClick(TObject *Sender)
{
	tc1->ActiveTab = tiButtons;
    int y = gpl2->Position->Y;
	gpl2 ->Position->Y = - gpl2->Height;
	TAnimator::AnimateIntWait(gpl2, "Position.Y", y, 1, TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buBack1Click(TObject *Sender)
{
    tc1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buLabelsStartClick(TObject *Sender)
{
	tc1->ActiveTab = tiLabels;
	laUniversitet ->Position->X = - laUniversitet->Width;
	laGroup ->Position->X = this->Width + laGroup->Width;
	laFIO->Position->X = - laFIO->Width;
	TAnimator::AnimateIntWait(laUniversitet, "Position.X", 0, 1, TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(laGroup, "Position.X", 0, 1, TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(laFIO, "Position.X", 0, 1, TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
    tc1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buStartLabelClick(TObject *Sender)
{
	tc1->ActiveTab = tiLabel;
	laLabel->AutoSize = true;
	laLabel->TextSettings->Font->Size = 400;
	TAnimator::AnimateIntWait(laLabel, "TextSettings.Font.Size", 96, 1, TAnimationType::Out, TInterpolationType::Linear);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buBack2Click(TObject *Sender)
{
	tc1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buBack3Click(TObject *Sender)
{
    tc1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAboutClick(TObject *Sender)
{
	ShowMessage(L"���������� �������� ����������� ������� ��������, ��������� ������ 141-322");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

