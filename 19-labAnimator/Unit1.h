//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc1;
	TTabItem *tiMain;
	TTabItem *tiButtons;
	TTabItem *tiLabels;
	TTabItem *tiLabel;
	TGridPanelLayout *gpl1;
	TButton *buStart;
	TLabel *laUniversitet;
	TLabel *laGroup;
	TLabel *laFIO;
	TToolBar *ToolBar1;
	TButton *buBack1;
	TButton *buLabelsStart;
	TButton *buStartLabel;
	TButton *buAbout;
	TButton *buExit;
	TLabel *laLabel;
	TGridPanelLayout *gpl2;
	TToolBar *ToolBar2;
	TButton *buBack2;
	TButton *Button1;
	TButton *Button2;
	TButton *Button5;
	TButton *Button6;
	TToolBar *ToolBar3;
	TButton *buBack3;
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buBack1Click(TObject *Sender);
	void __fastcall buLabelsStartClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buStartLabelClick(TObject *Sender);
	void __fastcall buBack2Click(TObject *Sender);
	void __fastcall buBack3Click(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buExitClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
