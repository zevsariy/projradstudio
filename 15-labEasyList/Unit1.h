//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiList;
	TTabItem *tiItem;
	TListBox *lb1;
	TImage *img;
	TText *tx;
	TToolBar *tb1;
	TButton *buBack;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TScrollBox *ScrollBox1;
	TStyleBook *StyleBook1;
	void __fastcall lb1ItemClick(TCustomListBox * const Sender, TListBoxItem * const Item);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);

private:	// User declarations
void LoadItem(int);
    System::UnicodeString FPath;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
