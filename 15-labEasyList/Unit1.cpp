//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
const UnicodeString cList[] = {
	"bird", "box", "ship"
};
//---------------------------------------------------------------------------
void TForm1::LoadItem(int aIndex)
{
	UnicodeString xName = System::Ioutils::TPath::Combine(FPath, cList[aIndex]);
	//
	img->Bitmap->LoadFromFile(xName+".jpg");
	TStringList *x = new TStringList;
	__try{
		x->LoadFromFile(xName+".txt");
		tx->Text = x->Text;
	}
	__finally{
		x->DisposeOf();
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::lb1ItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	ScrollBox1->ViewportPosition = TPointF(0,0);
	LoadItem(Item->Index);
	tx->RecalcSize();
	tc->ActiveTab = tiItem;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	FPath =
#ifdef _Windows
"..\\..\\res\\";
#else
System::Ioutils::TPath::GetDocumentsPath();
#endif
	tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buBackClick(TObject *Sender)
{
   tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------

