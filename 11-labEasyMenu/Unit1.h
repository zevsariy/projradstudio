//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Objects.hpp>
#include <FMX.WebBrowser.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *Main;
	TTabItem *Info;
	TTabItem *Feedback;
	TLayout *Layout1;
	TButton *BuInfo;
	TButton *BuFeedback;
	TButton *BuUniverSite;
	TButton *BuProfkomSite;
	TButton *BuExit;
	TToolBar *ToolBar1;
	TLabel *Label1;
	TButton *BuReturnFeedback;
	TGridPanelLayout *GridPanelLayout1;
	TToolBar *ToolBar2;
	TLabel *LaInfo;
	TButton *BuReturnInfo;
	TButton *BuAboutMe;
	TMemo *InfoText;
	TImage *Image1;
	TLayout *Layout2;
	TMemo *Memo1;
	TButton *BuSendFeedback;
	TTabItem *BrowserTab;
	TToolBar *ToolBar3;
	TLabel *LaBrowser;
	TButton *ReturnBuBrowser;
	TLayout *Layout3;
	TWebBrowser *WB;
	TStyleBook *StyleBook1;
	void __fastcall BuAboutMeClick(TObject *Sender);
	void __fastcall BuReturnAll(TObject *Sender);
	void __fastcall BuInfoClick(TObject *Sender);
	void __fastcall BuFeedbackClick(TObject *Sender);
	void __fastcall BuExitClick(TObject *Sender);
	void __fastcall BuSendFeedbackClick(TObject *Sender);
	void __fastcall BuProfkomSiteClick(TObject *Sender);
	void __fastcall BuUniverSiteClick(TObject *Sender);
	void __fastcall BuSendFeedbackKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
