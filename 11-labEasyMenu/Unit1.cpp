//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TForm1::BuAboutMeClick(TObject *Sender)
{
	ShowMessage(L"���������� ����������� ������� ��������, �������� ������ 141-322.");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BuReturnAll(TObject *Sender)
{
	tc->ActiveTab = Main;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BuInfoClick(TObject *Sender)
{
    tc ->ActiveTab = Info;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BuFeedbackClick(TObject *Sender)
{
    tc ->ActiveTab = Feedback;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BuExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::BuSendFeedbackClick(TObject *Sender)
{
	ShowMessage(L"��� ����� ������� ���������. �������!");
	tc->ActiveTab = Main;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BuProfkomSiteClick(TObject *Sender)
{
	WB->URL = "https://profkomum.ru/";
	WB ->StartLoading();
	tc->ActiveTab = BrowserTab;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BuUniverSiteClick(TObject *Sender)
{
	WB->URL = "https://mami.ru/";
	WB ->StartLoading();
	tc->ActiveTab = BrowserTab;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BuSendFeedbackKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if((tc->ActiveTab != Main) && (Key == vkHardwareBack))
	{
		tc->ActiveTab = Main;
		Key = 0;
    }
}
//---------------------------------------------------------------------------

