//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <System.Actions.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.MediaLibrary.Actions.hpp>
#include <FMX.StdActns.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TActionList *ActionList1;
	TToolBar *ToolBar1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TImage *Im;
	TAction *acClear;
	TTakePhotoFromLibraryAction *TakePhotoFromLibraryAction1;
	TTakePhotoFromCameraAction *TakePhotoFromCameraAction1;
	TShowShareSheetAction *ShowShareSheetAction1;
	void __fastcall acClearExecute(TObject *Sender);
	void __fastcall TakePhotoFromLibraryAction1DidFinishTaking(TBitmap *Image);
	void __fastcall ShowShareSheetAction1BeforeExecute(TObject *Sender);
	void __fastcall TakePhotoFromCameraAction1DidFinishTaking(TBitmap *Image);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
