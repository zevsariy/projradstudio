//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
	FDConnection->ExecSQL(
	" CREATE TABLE IF NOT EXISTS [Notes2]("
	" [title] VARCHAR(50) NOT NULL,"
	" [priority] SMALLINT NOT NULL,"
	" [description] VARCHAR(500))"
	);
    taNotes->Open();
}
//---------------------------------------------------------------------------
void __fastcall Tdm::FDConnectionBeforeConnect(TObject *Sender)
{
	FDConnection->Params->Values["Database"] =
#ifdef _Windows
 "..\\..\\notes.db" ;
#else
System::Ioutils::TPath::GetDocumentsPath() + PathDelim +  "notes.db" ;
#endif
}
//---------------------------------------------------------------------------
