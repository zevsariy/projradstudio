//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormShow(TObject *Sender)
{
	 dm->FDConnection->Connected = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAddClick(TObject *Sender)
{
	dm->taNotes->Append();
	tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->taNotes->Edit();
	tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buSaveClick(TObject *Sender)
{
	dm->taNotes->Post();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buCancelClick(TObject *Sender)
{
    dm->taNotes->Cancel();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buDeleteClick(TObject *Sender)
{
    dm->taNotes->Delete();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

