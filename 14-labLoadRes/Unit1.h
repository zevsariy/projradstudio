//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buFirst;
	TButton *buSecond;
	TButton *buThird;
	TButton *buInfo;
	TImage *img;
	TMemo *me1;
	TButton *buClear;
	TStyleBook *StyleBook1;
	void __fastcall buFirstClick(TObject *Sender);
	void __fastcall buSecondClick(TObject *Sender);
	void __fastcall buThirdClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
