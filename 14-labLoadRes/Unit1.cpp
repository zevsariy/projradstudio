//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void LoadresourceToImage(UnicodeString aResName, TBitmap* aResult)
{
	TResourceStream* x;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try{
		aResult->LoadFromStream(x);
	}
	__finally {
		x->Free();
	}
}

//---------------------------------------------------------------------------
UnicodeString LoadResourceToText(UnicodeString aResName)
{
	TResourceStream* x;
	TStringStream* xSS;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try{
		xSS = new TStringStream("", TEncoding::UTF8, true);
 //		xSS = new TStringStream();
		try{
			xSS->LoadFromStream(x);
			return xSS->DataString;
		}
		__finally {
			xSS->Free();
		}
	}
	__finally{
		x->Free();
	}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::buFirstClick(TObject *Sender)
{
	LoadresourceToImage("PngImage_1", img->Bitmap);
	me1->Lines->Text=LoadResourceToText("Resource_1");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buSecondClick(TObject *Sender)
{
	LoadresourceToImage("PngImage_2", img->Bitmap);
	me1->Lines->Text=LoadResourceToText("Resource_2");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buThirdClick(TObject *Sender)
{
	LoadresourceToImage("PngImage_3", img->Bitmap);
	me1->Lines->Text=LoadResourceToText("Resource_3");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buClearClick(TObject *Sender)
{
    img->Bitmap->SetSize(0,0);
	me1->Lines->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buInfoClick(TObject *Sender)
{
	ShowMessage(L"����-����������, ������������� ������� ��������, ��������� ������ 141-322");
}
//---------------------------------------------------------------------------

