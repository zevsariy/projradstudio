//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Edit.hpp>
#include <System.Sensors.Components.hpp>
#include <System.Sensors.hpp>
#include <FMX.Colors.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class TFm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *Main;
	TGridPanelLayout *GPL;
	TLabel *LaMain;
	TImage *Logo_Image;
	TButton *BuSee;
	TButton *BuAdd;
	TButton *Exit;
	TButton *Button1;
	TTabItem *SeeTab;
	TTabItem *AddTab;
	TToolBar *ToolBar1;
	TLabel *SeeLa;
	TButton *SeeBuReturn;
	TLayout *Layout1;
	TMemo *MindMemo;
	TButton *BuClean;
	TToolBar *TbAdd;
	TLabel *LaAdd;
	TButton *ReturnBuAdd;
	TLayout *Layout2;
	TButton *BuAddMind;
	TEdit *MyMindText;
	TLabel *LaTextInput;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall BuSeeClick(TObject *Sender);
	void __fastcall BuAddClick(TObject *Sender);
	void __fastcall ExitClick(TObject *Sender);
	void __fastcall BuCleanClick(TObject *Sender);
	void __fastcall ReturnBuAll(TObject *Sender);
	void __fastcall BuAddMindClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFm *Fm;
//---------------------------------------------------------------------------
#endif
