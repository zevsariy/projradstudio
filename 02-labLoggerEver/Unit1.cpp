//---------------------------------------------------------------------------

#include <fmx.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
int randa;
char a[11] = {'1','2','3','4','5','6','7','8','9','10','11'};
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BumClick(TObject *Sender)
{
	int secret = rand() % 10 + 1;

	Logger ->Lines ->Add("�������� �������� " + a[secret]);
	ShowMessage(a[secret]);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Logger ->Lines ->Add("Button1.Click");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    Logger ->Lines ->Add(Button2->Name + ".Click");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	Logger ->Lines->Add(((TControl *)Sender)->Name + ".Click");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ResetClick(TObject *Sender)
{
	Logger->Lines->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	Logger->Lines->Add(Sender->ToString() + ".Click");
}
//---------------------------------------------------------------------------
