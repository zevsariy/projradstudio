//---------------------------------------------------------------------------

#pragma hdrstop

#include "PasswordGen.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

System::UnicodeString RandomStr(int aLength, bool aLower, bool aUpper, bool aNumber, bool aSpec)
{
	const char *c1 = "qwertyuiopasfghjklzxcvbnm";
	const char *c2 = "0123456789";
	const char *c3 = "[]{}<>,.;:-+#";

	System::UnicodeString x="";
	System::UnicodeString xRezult="";

	if (aLower)
	{
		x += c1;
	}
	 if (aUpper)
	{
		x += UpperCase(c1);
	}
	if (aNumber)
	{
		x += c2;
	}
	if (aSpec)
	{
		x += c3;
	}
	if (x.IsEmpty())
	{
		x = c1;
	}
	while (xRezult.Length() < aLength)
	{
		xRezult += x.SubString(Random(x.Length() + 1), 1);
	}
	return xRezult;
}
