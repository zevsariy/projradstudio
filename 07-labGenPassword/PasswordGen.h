//---------------------------------------------------------------------------

#ifndef PasswordGenH
#define PasswordGenH
//---------------------------------------------------------------------------
#endif

#include <System.Classes.hpp>

System::UnicodeString RandomStr(int aLength, bool aLower, bool aUpper, bool aNumber, bool aSpec);
