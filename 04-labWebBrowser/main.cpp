//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
    WB1 -> GoBack();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BuGoClick(TObject *Sender)
{
	WB1 -> URL = edURL -> Text;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    WB1 -> GoForward();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
    WB1 ->Reload();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
    WB1 ->Stop();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
	ShowMessage("������ ���� ������� ������������ ������� ��������");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::edURLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if(Key == vkReturn)
	{
		WB1 ->URL = edURL ->Text;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::WB1DidFinishLoad(TObject *ASender)
{
    edURL ->Text = WB1 ->URL;
}
//---------------------------------------------------------------------------
