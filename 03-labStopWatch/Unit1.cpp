//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BuStartClick(TObject *Sender)
{
	FDateTime = Now();
	Timer ->Enabled = True;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BuStopClick(TObject *Sender)
{
	Timer -> Enabled = False;
	FDateTime = Now();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TimerTimer(TObject *Sender)
{
	Time ->Text = TimeToStr (Now() - FDateTime);
}
//---------------------------------------------------------------------------
