//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *BuStart;
	TButton *BuStop;
	TLabel *Time;
	TTimer *Timer;
	void __fastcall BuStartClick(TObject *Sender);
	void __fastcall BuStopClick(TObject *Sender);
	void __fastcall TimerTimer(TObject *Sender);
private:	// User declarations
    TDateTime FDateTime;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
