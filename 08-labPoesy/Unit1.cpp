//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}

inline int Low(const System::UnicodeString &)
{
#ifdef _DELPHI_STRING_ONE_BASED
	return 1;
#else
	return 0;
#endif
}

inline int High(const System::UnicodeString &S)
{
	#ifdef _DELPHI_STRING_ONE_BASED
		return S.Length();
	#else
		return S.Length()-1;
	#endif
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	ShowMessage(L"���������� ������������� ������� ��������.");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Memo1 ->TextSettings->Font->Size -=1;
	Memo2 ->TextSettings->Font->Size -=1;
	Memo3 ->TextSettings->Font->Size -=1;
	Memo4 ->TextSettings->Font->Size -=1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	Memo1 ->TextSettings->Font->Size +=1;
	Memo2 ->TextSettings->Font->Size +=1;
	Memo3 ->TextSettings->Font->Size +=1;
	Memo4 ->TextSettings->Font->Size +=1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	System::UnicodeString x;
	//����� �������
	for (int i = 0; i < Memo1->Lines->Count; i++) {
		x = Memo1->Lines->Strings[i];
		if (i % 2 == 1) {
			for (int j = Low(x); j <= High(x);  j++) {
				if (x[j] != ' ') {
					x[j] = '*';
				}
			}
		}
		Memo2->Lines->Add(x);
	}

	//������ �����
	bool xFlag;
	for (int i = 0; i < Memo1->Lines->Count; i++) {
		x = Memo1->Lines->Strings[i];
		xFlag = false;
		for (int j = Low(x); j <= High(x);  j++) {
			if ((xFlag) && (x[j] != ' ')){
				x[j] = 'x';
			}
			if ((!xFlag) && (x[j] == ' ')){
				xFlag = true;
			}

		}
		Memo3->Lines->Add(x);
	}
}
//---------------------------------------------------------------------------

