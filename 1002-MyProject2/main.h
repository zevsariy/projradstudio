//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb1;
	TButton *buInfo;
	TGridPanelLayout *gpl;
	TButton *buAuth;
	TButton *buFAQ;
	TButton *buExit;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiSkills;
	TTabItem *tiSkill;
	TListView *lvSkills;
	TToolBar *tb2;
	TToolBar *tb3;
	TButton *buToSkills;
	TButton *buToMenu;
	TTabItem *tiNewSkill;
	TToolBar *tb4;
	TButton *buToSkill;
	TLabel *laTitleNewSkill;
	TLabel *laTitleSkill;
	TLabel *laTitleSkills;
	TLabel *laTitleMenu;
	TLabel *laSkillName;
	TLabel *laSkillTimer;
	TButton *buSkillStartStop;
	TTabItem *tiAuth;
	TToolBar *tb5;
	TButton *buToMenuFromAuth;
	TLabel *laError;
	TLabel *laPassword;
	TEdit *edLogin;
	TEdit *edPassword;
	TButton *buAuthSend;
	TLayout *lyAuth;
	TLabel *laLogin;
	TButton *buAddNewSkill;
	TLayout *ly2;
	TTimer *tmSkill;
	TTabItem *tiInfo;
	TToolBar *tb6;
	TButton *buBackFromInfo;
	TLabel *laInfoTitle;
	TText *tText;
	TLayout *ly3;
	TLabel *laSkillNameHere;
	TButton *buAddSkill;
	TEdit *edNewSkillName;
	TStyleBook *StyleBook1;
	TMemo *meUserId;
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall buToMenuClick(TObject *Sender);
	void __fastcall buToSkillsClick(TObject *Sender);
	void __fastcall buToSkillClick(TObject *Sender);
	void __fastcall buToMenuFromAuthClick(TObject *Sender);
	void __fastcall buAuthSendClick(TObject *Sender);
	void __fastcall buAddNewSkillClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvSkillsItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall tmSkillTimer(TObject *Sender);
	void __fastcall buSkillStartStopClick(TObject *Sender);
	void __fastcall buAuthClick(TObject *Sender);
	void __fastcall buFAQClick(TObject *Sender);
	void __fastcall buBackFromInfoClick(TObject *Sender);
	void __fastcall buAddSkillClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	int user_id;
	void GetSkills();
	void AddNewSkill();
	void UpdateTimeOnServer();
	int Time_Counter;
	int skill_id;
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
