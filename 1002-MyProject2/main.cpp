#include <fmx.h>
#pragma hdrstop

#include "main.h"
#include "dm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buInfoClick(TObject *Sender)
{
    ShowMessage(L"���������� ��������� ����������� Zevsariy Studio(������ ��������).");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buToMenuClick(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buToSkillsClick(TObject *Sender)
{
if(tmSkill->Enabled)
{
	tmSkill->Enabled = false;
	buSkillStartStop->Text = L"������ ������";
	UpdateTimeOnServer();
}
	GetSkills();
	tc->ActiveTab = tiSkills;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buToSkillClick(TObject *Sender)
{
    tc->ActiveTab = tiSkill;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buToMenuFromAuthClick(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void Tfm::GetSkills()
{
		TJSONObject *JSON_Req = new TJSONObject();
		TJSONObject *JSON_Data = new TJSONObject();

		JSON_Req->AddPair( new TJSONPair("action", "get"));
		JSON_Req->AddPair( new TJSONPair("module", "skills"));
		JSON_Req->AddPair( new TJSONPair("data", JSON_Data));
		JSON_Data->AddPair( new TJSONPair("user_id", user_id) );
		dm->req->AddParameter("json_data", JSON_Req->ToString());
		dm->req->Execute();


		TJSONObject* JSON_Resp(static_cast<TJSONObject*>(TJSONObject::ParseJSONValue(TEncoding::UTF8->GetBytes(dm->resp->JSONText),0)));
		TJSONObject* J_Data = static_cast<TJSONObject*>(JSON_Resp->Get("data")->JsonValue);
		TJSONArray* J_Skills = static_cast<TJSONArray*>(J_Data->Get("skills")->JsonValue);
		TJSONObject* J_Item;
		lvSkills->Items->Clear();
		for(int i=0; i < J_Skills->Count; i++)
		{
			J_Item = static_cast<TJSONObject*>(J_Skills->Get(i));

			TListViewItem *Skill_Item;
			Skill_Item = lvSkills->Items->Add();
			Skill_Item->Tag =  StrToInt(J_Item->GetValue("id")->ToString());
			Skill_Item->Text = J_Item->GetValue("name")->ToString();
		}
}


void Tfm::UpdateTimeOnServer()
{
		TJSONObject *JSON_Req = new TJSONObject();
		TJSONObject *JSON_Data = new TJSONObject();

		JSON_Req->AddPair( new TJSONPair("action", "update"));
		JSON_Req->AddPair( new TJSONPair("module", "skills"));
		JSON_Req->AddPair( new TJSONPair("data", JSON_Data));
		JSON_Data->AddPair( new TJSONPair("user_id", user_id) );
		JSON_Data->AddPair( new TJSONPair("skill_id", skill_id) );
		JSON_Data->AddPair( new TJSONPair("time", Time_Counter) );
		dm->req->AddParameter("json_data", JSON_Req->ToString());
		dm->req->Execute();
}

void Tfm::AddNewSkill()
{
		TJSONObject *JSON_Req = new TJSONObject();
		TJSONObject *JSON_Data = new TJSONObject();

		JSON_Req->AddPair( new TJSONPair("action", "add"));
		JSON_Req->AddPair( new TJSONPair("module", "skills"));
		JSON_Req->AddPair( new TJSONPair("data", JSON_Data));
		JSON_Data->AddPair( new TJSONPair("user_id", user_id) );
		JSON_Data->AddPair( new TJSONPair("skill_name", edNewSkillName->Text) );
		dm->req->AddParameter("json_data", JSON_Req->ToString());
		dm->req->Execute();

		TJSONObject* JSON_Resp(static_cast<TJSONObject*>(TJSONObject::ParseJSONValue(TEncoding::UTF8->GetBytes(dm->resp->JSONText),0)));
		TJSONObject* J_Data = static_cast<TJSONObject*>(JSON_Resp->Get("data")->JsonValue);
        ShowMessage(J_Data->ToString());
}

void __fastcall Tfm::buAuthSendClick(TObject *Sender)
{
	UnicodeString Login = edLogin->Text;
	UnicodeString Pass = edPassword->Text;

	//������������ json � ������� � �������
	if (Login.Length() == 0 || Pass.Length() == 0) {
		laError->Text = L"��� ���� ������ ���� ���������";
	}else
	{
		//�������� ���������� � ����������� ��� �����������
		TJSONObject *JsonToSend = new TJSONObject();
		TJSONObject *jdata = new TJSONObject();

		JsonToSend->AddPair( new TJSONPair("action", "login"));
		JsonToSend->AddPair( new TJSONPair("module", "auth"));
		JsonToSend->AddPair( new TJSONPair("data", jdata));
		jdata->AddPair( new TJSONPair("login", Login) );
		jdata->AddPair( new TJSONPair("password",Pass) );
		dm->req->AddParameter("json_data", JsonToSend->ToString());
		dm->req->Execute();

		//TJSONObject* c = (TJSONObject*) TJSONObject::ParseJSONValue(TEncoding::UTF8->GetBytes(dm->resp->JSONText),0);

		//TJSONObject* data = (TJSONObject*) c->Get("data")->JsonValue;


		TJSONObject *JSON = (TJSONObject*)TJSONObject::ParseJSONValue(TEncoding::UTF8->GetBytes(dm->resp->JSONText),0);
		TJSONPair *pair = JSON->Get("data");
		TJSONObject *jsonObj = (TJSONObject*) pair->JsonValue;
		TJSONPair *test21 = jsonObj->Get("user_id");
		String j_user_id = test21->JsonValue->ToString();


		//String user_id_new = c->Value("user_id");
		//String j_user_id = data->GetValue("user_id");
	   //	String j_user_id = data->Pairs[1]->JsonValue->ToString();

		if (StrToInt(j_user_id) != 0) {
			user_id = StrToInt(j_user_id);
			tc->ActiveTab = tiSkills;
			GetSkills();
			meUserId ->Lines ->Clear();
			meUserId->Lines->Add(IntToStr(user_id));
		}else
		{
			laError->Text = L"�������� ����� ��� ������";
			edLogin->Text = "";
			edPassword->Text = "";
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAddNewSkillClick(TObject *Sender)
{
	tc->ActiveTab = tiNewSkill;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvSkillsItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	TJSONObject* JSON_Resp(static_cast<TJSONObject*>(TJSONObject::ParseJSONValue(TEncoding::UTF8->GetBytes(dm->resp->JSONText),0)));
	TJSONObject* J_Data = static_cast<TJSONObject*>(JSON_Resp->Get("data")->JsonValue);
	TJSONArray* J_Skills = static_cast<TJSONArray*>(J_Data->Get("skills")->JsonValue);
	TJSONObject* J_Item;
	lvSkills->Items->Clear();
	Time_Counter = 0;
	for(int i=0; i < J_Skills->Count; i++)
	{
		J_Item = static_cast<TJSONObject*>(J_Skills->Get(i));
		int jtag = StrToInt(J_Item->GetValue("id")->ToString());
		if(jtag == AItem->Tag)
		{
		   Time_Counter = StrToInt(J_Item->GetValue("time")->ToString());
		   laSkillTimer->Text = IntToStr(Time_Counter) + " ����� � ������";
		   laSkillName->Text = J_Item->GetValue("name")->ToString();
		}
	}
	tc->ActiveTab = tiSkill;
	skill_id = AItem->Tag;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tmSkillTimer(TObject *Sender)
{
	Time_Counter += 1;
	laSkillTimer->Text = IntToStr(Time_Counter) + " ����� � ������";
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSkillStartStopClick(TObject *Sender)
{
	if(tmSkill->Enabled)
	{
		tmSkill->Enabled = false;
		buSkillStartStop->Text = L"������ ������";
		UpdateTimeOnServer();
	}
	else
	{
		tmSkill->Enabled = true;
		buSkillStartStop->Text = L"���������� ������";
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAuthClick(TObject *Sender)
{
    tc->ActiveTab = tiAuth;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buFAQClick(TObject *Sender)
{
    tc->ActiveTab = tiInfo;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buBackFromInfoClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAddSkillClick(TObject *Sender)
{
	if(edNewSkillName->Text == "")
	{
         laSkillNameHere->Text = L"������� ��������� ��� ������, � ����� �����������";
	}
	else
	{
		AddNewSkill();
		GetSkills();
		tc->ActiveTab = tiSkills;
	}
}
//---------------------------------------------------------------------------


