//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "resUn.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm1 *fm1;
//---------------------------------------------------------------------------
__fastcall Tfm1::Tfm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::JSON1Click(TObject *Sender)
{
	RESTRequest3->Execute();
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::JSON2Click(TObject *Sender)
{
     RESTRequest1->Execute();
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::Button3Click(TObject *Sender)
{
    RESTRequest2->Execute();
}
//---------------------------------------------------------------------------
