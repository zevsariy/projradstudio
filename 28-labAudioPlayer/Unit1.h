//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Media.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <System.IOUtils.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *lyTop;
	TLayout *lyCurrentTime;
	TLayout *lyBottom;
	TLabel *laTitle;
	TButton *buAbout;
	TListView *lv;
	TMediaPlayer *mp;
	TTimer *tm;
	TTrackBar *tbCurrentTime;
	TLabel *laCurrentTime;
	TLabel *laDuration;
	TButton *buPlay;
	TButton *buPause;
	TButton *buStop;
	TButton *buPrev;
	TButton *buNext;
	TLabel *laPath;
	TStyleBook *StyleBook1;
	TTrackBar *tbVolume;
	TButton *buRepeate;
	TButton *buRand;
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buPauseClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall tbCurrentTimeChange(TObject *Sender);
	void __fastcall buPrevClick(TObject *Sender);
	void __fastcall buNextClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall tbVolumeChange(TObject *Sender);
	void __fastcall buRepeateClick(TObject *Sender);
	void __fastcall buRandClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	int FPauseTime;
	void DoRefreshList(UnicodeString aPath);
	void DoStop();
	bool Repeate;
    bool RandomizerRevers;
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
