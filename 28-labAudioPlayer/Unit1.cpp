//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    ShowMessage(L"����������-����� ����������� ������� ��������, ������ 141-322");
}
//---------------------------------------------------------------------------
void Tfm::DoRefreshList(UnicodeString aPath)
{
	laPath->Text = aPath;
	DynamicArray<UnicodeString> x;
	TListViewItem *xItem;
	x = System::Ioutils::TDirectory::GetFiles(aPath, "*.mp3");
	lv->BeginUpdate();
	if(!RandomizerRevers)
	{
		try
		{
            lv->Items->Clear();
			for(int i=0; i < x.Length; i++)
			{
				xItem = lv->Items->Add();
				xItem-> Text = System::Ioutils::TPath::GetFileNameWithoutExtension(x[i]);
				xItem->Detail = x[i];
			}
		}
		__finally
		{
			lv->EndUpdate();
		}
	}
	else
	{
		try
		{
            lv->Items->Clear();
			for(int i = x.Length-1; i >= 0; i--)
			{
				xItem = lv->Items->Add();
				xItem-> Text = System::Ioutils::TPath::GetFileNameWithoutExtension(x[i]);
				xItem->Detail = x[i];
			}
		}
		__finally
		{
			lv->EndUpdate();
		}
    }
}

void Tfm::DoStop()
{
	mp->Stop();
	tm->Enabled = false;
	lv->ItemIndex = -1;
	tbCurrentTime->Value = 0;
	FPauseTime = 0;
}
void __fastcall Tfm::FormCreate(TObject *Sender)
{
    mp->Volume = 1;
	FPauseTime = 0;
	Repeate = false;
	RandomizerRevers = false;
	DoRefreshList(System::Ioutils::TPath::GetSharedMusicPath());
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
    buPlayClick(this);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPlayClick(TObject *Sender)
{
	if(lv->ItemIndex != -1)
	{
		tm->Enabled = false;
		mp->Clear();
		mp->FileName = lv->Items->AppearanceItem[lv->ItemIndex]->Detail;
		tbCurrentTime->Enabled = true;
		tbCurrentTime->Max = mp->Duration;
		laDuration->Text = Format("%2.2d:%2.2d", ARRAYOFCONST((
			(unsigned int)mp->Duration/MediaTimeScale/60,
			(unsigned int)mp->Duration/MediaTimeScale % 60
			)));
		mp->CurrentTime = FPauseTime;
		FPauseTime = 0;
		mp->Play();
        tm->Enabled = true;
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPauseClick(TObject *Sender)
{
	if(mp->State == TMediaState::Playing)
	{
		FPauseTime = mp->Media->CurrentTime;
		mp->Stop();
	}
	else
	{
        buPlayClick(this);
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStopClick(TObject *Sender)
{
	if(mp->State == TMediaState::Playing)
	{
        DoStop();
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tbCurrentTimeChange(TObject *Sender)
{
	if(tbCurrentTime->Tag == 0)
	{
		if((mp->State == TMediaState::Stopped)&&(FPauseTime != 0))
		{
			FPauseTime = tbCurrentTime->Value;
		}
		else
		{
            mp->CurrentTime = tbCurrentTime->Value;
        }
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPrevClick(TObject *Sender)
{
	if((lv->ItemIndex != -1) && (lv->ItemIndex > 0))
	{
		lv->ItemIndex = lv->ItemIndex -1;
		FPauseTime = 0;
		buPlayClick(this);
	}
	else
	{
		DoStop();
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buNextClick(TObject *Sender)
{
	if((lv->ItemIndex != -1) && (lv->ItemIndex < lv->ItemCount-1))
	{
		lv->ItemIndex = lv->ItemIndex +1;
		FPauseTime = 0;
		buPlayClick(this);
	}
	else
	{
		if(Repeate)
		{
			lv->ItemIndex = 0;
			FPauseTime = 0;
			buPlayClick(this);
        }
		else
		{
			DoStop();
		}
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tmTimer(TObject *Sender)
{
	tbCurrentTime->Tag = 1;
	tbCurrentTime->Value = mp->CurrentTime;
	tbCurrentTime->Tag = 0;
	laCurrentTime->Text = Format("%2.2d:%2.2d", ARRAYOFCONST((
		(unsigned int)mp->CurrentTime / MediaTimeScale / 60,
		(unsigned int)mp->CurrentTime / MediaTimeScale % 60
	)));
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tbVolumeChange(TObject *Sender)
{
    mp->Volume = tbVolume->Value;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRepeateClick(TObject *Sender)
{
	Repeate = !Repeate;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRandClick(TObject *Sender)
{
	RandomizerRevers = !RandomizerRevers;
    DoRefreshList(System::Ioutils::TPath::GetSharedMusicPath());
}
//---------------------------------------------------------------------------
