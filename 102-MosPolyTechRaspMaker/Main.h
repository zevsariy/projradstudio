//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.WebBrowser.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiGroups;
	TTabItem *tiRasp;
	TToolBar *ToolBar1;
	TToolBar *tbGroups;
	TToolBar *ToolBar3;
	TButton *buSelectGroup;
	TEdit *edGroupName;
	TButton *buReturnGroups;
	TListView *lvGroups;
	TWebBrowser *wbRasp;
	void __fastcall buSelectGroupClick(TObject *Sender);
	void __fastcall buReturnGroupsClick(TObject *Sender);
	void __fastcall edGroupNameChange(TObject *Sender);
	void __fastcall lvGroupsItemClick(TObject * const Sender, TListViewItem * const AItem);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
