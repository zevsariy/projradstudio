//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Main.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSelectGroupClick(TObject *Sender)
{
	tc->ActiveTab = tiGroups;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buReturnGroupsClick(TObject *Sender)
{
    tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::edGroupNameChange(TObject *Sender)
{
	//dm->req->ClearBody();
	dm->req->Params->Clear();
	dm->req->AddParameter("module", "rasp");
	//dm->req->AddParameter("module", edGroupName->Text);
	dm->req->Execute();
	ShowMessage(dm->resp->JSONText);

	/*TJSONObject* JSON_Resp(static_cast<TJSONObject*>(TJSONObject::ParseJSONValue(TEncoding::UTF8->GetBytes(dm->resp->JSONText),0)));
		TJSONArray* J_Data = static_cast<TJSONArray*>(JSON_Resp->Get("data")->JsonValue);
		TJSONObject* J_Item;
		lvGroups->Items->Clear();
		for(int i=0; i < J_Data->Count; i++)
		{
			J_Item = static_cast<TJSONObject*>(J_Data->Get(i));
			TListViewItem *GroupItem;
			GroupItem = lvGroups->Items->Add();
			GroupItem->Text = J_Item->GetValue("Group")->Value();
		}
        */
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvGroupsItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	//dm->req->ClearBody();
	dm->req->Params->Clear();
	dm->req->AddParameter("group", "141-322");
	dm->req->Execute();
	ShowMessage(dm->resp->Content);

	tc->ActiveTab = tiRasp;
}
//---------------------------------------------------------------------------
