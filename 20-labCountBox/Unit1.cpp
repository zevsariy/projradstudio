//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm1 *fm1;
//---------------------------------------------------------------------------
__fastcall Tfm1::Tfm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::FormCreate(TObject *Sender)
{
	tc1->ActiveTab = tiMain;
	FListBox = new TList;
	for (int i=1; i <= cMaxBox; i++) {
		 FListBox->Add(this->FindComponent("Rectangle" + IntToStr(i)));
	}
	FListAnswer = new TList;
	FListAnswer->Add(buAnswer1);
	FListAnswer->Add(buAnswer2);
	FListAnswer->Add(buAnswer3);
	FListAnswer->Add(buAnswer4);
	FListAnswer->Add(buAnswer5);
	FListAnswer->Add(buAnswer6);
	laLife->Text = "5";
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::buStartClick(TObject *Sender)
{
	tc1->ActiveTab = tiGame;
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::buExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::buAboutClick(TObject *Sender)
{
	ShowMessage(L"���������� ����������� ������� ��������, ��������� ������ 141-322");
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::FormDestroy(TObject *Sender)
{
	delete FListBox;
	delete FListAnswer;
}

void Tfm1::DoContinue()
{
	for (int i =0; i < cMaxBox; i++) {
	   TRectangle *xRectangle;
	   xRectangle = (TRectangle*)FListBox->Items[i];
	   xRectangle->Fill->Color = TAlphaColorRec::Lightgray;
	}
	FNumberCorrect = RandomRange(cMinPossible, cMaxPossible);
	int *x = RandomArrayUnique(cMaxBox, FNumberCorrect);
	TRectangle *xRectangle;
	for(int i = 0; i < FNumberCorrect; i++)
	{
		xRectangle = (TRectangle*)FListBox->Items[x[i]];
		xRectangle->Fill->Color = TAlphaColorRec::Aqua;
	}
	int xAnswerStart = FNumberCorrect - Random(cMaxAnswer-1);
	if(xAnswerStart < cMinPossible)
		xAnswerStart = cMinPossible;

	for (int i = 0; i < cMaxAnswer; i++)
	{
		TButton *xButton;
		 xButton = (TButton*)FListAnswer->Items[i];
		 xButton->Text = IntToStr(xAnswerStart + i);
	}
}

void Tfm1::DoFinish()
{
	tm1->Enabled = false;
	laFinishCorrect->Text = L"���������� ������� - " + IntToStr( FCountCorrect);
	laFinishWrong->Text = L"������������ ������� - " + IntToStr( FCountWrong);
	tc1->ActiveTab = tiFinish;
	meRez ->Lines->Add(L"���������� " + IntToStr( FCountCorrect) + L" ������������ " + IntToStr( FCountWrong));
}



void Tfm1::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (float)30/(24*60*60);
	tm1->Enabled = true;
	DoContinue();
	laLife->Text = "5";
}
//---------------------------------------------------------------------------

void Tfm1::DoAnswer(int aValue)
{
   (aValue == FNumberCorrect) ? FCountCorrect++:FCountWrong++;
   if(aValue == FNumberCorrect)
		DoContinue();
   if(FCountWrong >= 5)
   {
	   ShowMessage(L"�� ���������");
	   DoFinish();
   }
   int Life = 5 - FCountWrong;
   laLife->Text = IntToStr(Life);
}

void __fastcall Tfm1::buAnswerAllClick(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::buResetClick(TObject *Sender)
{
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::tm1Timer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime ->Text = FormatDateTime("nn:ss", x);
	if(x <=0)
        DoFinish();
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::buReturnFinishClick(TObject *Sender)
{
	 tc1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::buRezaltClick(TObject *Sender)
{
     tc1->ActiveTab = tiRezult;
}
//---------------------------------------------------------------------------

