//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
#include "Unit2.h"
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc1;
	TTabItem *tiMain;
	TTabItem *tiGame;
	TTabItem *tiFinish;
	TTabItem *tiAbout;
	TGridPanelLayout *gpl1;
	TButton *buStart;
	TButton *buRezalt;
	TButton *buAbout;
	TButton *buExit;
	TToolBar *tb1;
	TGridPanelLayout *gpl2;
	TButton *buReset;
	TLabel *laTime;
	TLabel *laTitle;
	TLabel *laLife;
	TGridPanelLayout *gpl3;
	TButton *buAnswer1;
	TButton *buAnswer2;
	TButton *buAnswer3;
	TButton *buAnswer4;
	TButton *buAnswer5;
	TButton *buAnswer6;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TTimer *tm1;
	TTabItem *tiRezult;
	TToolBar *ToolBar1;
	TButton *buReturnFinish;
	TLabel *laFinishTitle;
	TLabel *laFinishCorrect;
	TLabel *laFinishText1;
	TLabel *laFinishWrong;
	TToolBar *ToolBar2;
	TButton *buReturnAbout;
	TLabel *Label1;
	TMemo *meTextAbout;
	TToolBar *ToolBar3;
	TButton *Button1;
	TLabel *laTitleRezult;
	TMemo *meRez;
	TStyleBook *StyleBook1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall buAnswerAllClick(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
	void __fastcall tm1Timer(TObject *Sender);
	void __fastcall buReturnFinishClick(TObject *Sender);
	void __fastcall buRezaltClick(TObject *Sender);
private:
	int FCountCorrect;
	int FCountWrong;
	int FNumberCorrect;
	double FTimeValue;
	TList *FListBox;
	TList *FListAnswer;
	void DoReset();
	void DoContinue();
	void DoAnswer(int aValue);
	void DoFinish();
public:		// User declarations
	__fastcall Tfm1(TComponent* Owner);
};
const int cMaxBox = 25;
const int cMaxAnswer = 6;
const int cMinPossible = 4;
const int cMaxPossible = 14;
//---------------------------------------------------------------------------
extern PACKAGE Tfm1 *fm1;
//---------------------------------------------------------------------------
#endif
