//---------------------------------------------------------------------------

#ifndef ProgrammH
#define ProgrammH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TButton *buStart;
	TTimer *tm;
	TTrackBar *tbTime;
	TLabel *laTitle;
	TLabel *laTime;
	TToolBar *tbMain;
	TLabel *laProgrammTitle;
	TButton *buAbout;
	TStyleBook *sb1;
	void __fastcall tbTimeChange(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
