//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Programm.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tbTimeChange(TObject *Sender)
{
	laTime->Text = FloatToStr(tbTime->Value) + " ������";
	tm->Interval = tbTime->Value * 1000;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
	if(tm->Enabled)
	{
		tm->Enabled = false;
		buStart->Text = "�����";
	}
	else
	{
		 tm->Enabled = true;
		 buStart->Text = "����������";
	}

}
//---------------------------------------------------------------------------
void __fastcall Tfm::tmTimer(TObject *Sender)
{
	dm->req->Execute();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    ShowMessage(L"���������� ����������� ����� ���� ������� ������� ��������. ���, ���� ���� ������. ����������� ������� �������� Zevsariy Studio");
}
//---------------------------------------------------------------------------
