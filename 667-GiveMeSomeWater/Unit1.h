//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Layouts.hpp>
#include <System.Notification.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *TB1;
	TLabel *laTitle;
	TButton *buInfo;
	TLayout *Layout1;
	TFloatAnimation *FloatAnimation1;
	TNotificationCenter *NotifCenter1;
	TTimer *Timer1;
	TLabel *laQuestion;
	TTrackBar *TrackBar1;
	TButton *buStartReset;
	TEdit *edText;
	TLabel *Label1;
	TLabel *laValue;
	TStyleBook *StyleBook1;
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall buStartResetClick(TObject *Sender);
	void __fastcall TrackBar1Change(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm1 *fm1;
//---------------------------------------------------------------------------
#endif
