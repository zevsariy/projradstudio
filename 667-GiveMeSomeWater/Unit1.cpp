//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm1 *fm1;
//---------------------------------------------------------------------------
__fastcall Tfm1::Tfm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::buInfoClick(TObject *Sender)
{
    ShowMessage(L"���������� ����������� ������� ��������");
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::Timer1Timer(TObject *Sender)
{
	TNotification *MyNotification = NotifCenter1->CreateNotification();
	MyNotification ->Title = edText->Text;
	MyNotification ->FireDate = Now();
	NotifCenter1->PresentNotification(MyNotification);
}
//---------------------------------------------------------------------------

void __fastcall Tfm1::buStartResetClick(TObject *Sender)
{
	if(Timer1 ->Enabled == true)
	{
		buStartReset ->Text = L"�����";
		Timer1 ->Enabled = false;
	}
	else
	{
		buStartReset ->Text = L"��������";
		Timer1 ->Enabled = true;
    }

}
//---------------------------------------------------------------------------


void __fastcall Tfm1::TrackBar1Change(TObject *Sender)
{
	int NewTime = TrackBar1 ->Value;
	laValue->Text = L"������ " + IntToStr(NewTime) + L" �����";
	Timer1 ->Interval = TrackBar1 ->Value * 60000;
}
//---------------------------------------------------------------------------

