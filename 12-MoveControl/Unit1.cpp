//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Circle1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
	FX = X;
	FY = Y;
	Dragging = true;
	((TControl *) Sender) ->BringToFront();
	((TControl *) Sender) ->Root->Captured = interface_cast<IControl>(Sender);
	if(dynamic_cast<TShape*>(Sender))
	{
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightblue;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Circle1MouseEnter(TObject *Sender)
{
    if(dynamic_cast<TShape*>(Sender))
	{
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Red;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Circle1MouseLeave(TObject *Sender)
{
	if(dynamic_cast<TShape*>(Sender))
	{
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightgreen;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Circle1MouseMove(TObject *Sender, TShiftState Shift, float X,
          float Y)
{
	if(Dragging && Shift.Contains(ssLeft))
	{
		((TControl *)Sender) ->Position->X += X - FX;
		((TControl *)Sender) ->Position->Y += Y - FY;
	}
	if(dynamic_cast<TShape*>(Sender))
	{
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightpink;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Circle1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
	Dragging = false;
    ((TControl *)Sender)->Root->Captured = 0;
	if(dynamic_cast<TShape*>(Sender))
	{
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightslategray;
	}
}
//---------------------------------------------------------------------------
