//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TCircle *Circle1;
	TCircle *Circle2;
	TCircle *Circle3;
	void __fastcall Circle1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall Circle1MouseEnter(TObject *Sender);
	void __fastcall Circle1MouseLeave(TObject *Sender);
	void __fastcall Circle1MouseMove(TObject *Sender, TShiftState Shift, float X, float Y);
	void __fastcall Circle1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);

private:	// User declarations
		float FX, FY;
        bool Dragging;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
