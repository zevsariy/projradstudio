//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "remembernumber.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFm *Fm;
//---------------------------------------------------------------------------
__fastcall TFm::TFm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
 void TFm::DoReset()
 {
	FCountCorrent = 0;
	FCountWrong = 0;
	DoContinue();
 }
//---------------------------------------------------------------------------
void TFm::DoContinue()
{
	laCorrent->Text = "���������� " + IntToStr(FCountCorrent);
	laWrong->Text = "������������ " + IntToStr(FCountWrong);
	FSecretNumber = RandomRange(100000, 999999);
  	laNumber->Text = IntToStr(FSecretNumber);
	edAnswer->Text = "";
	pbRemember->Value = 0;
	DoViewQuestion(true);
}
//---------------------------------------------------------------------------
void TFm::DoAnswer()
{
	int x = StrToIntDef(edAnswer->Text, 0);
	if (x == FSecretNumber){
		FCountCorrent++;
	}else {
		FCountWrong++;
	};
	DoContinue();

}
//---------------------------------------------------------------------------
void TFm::DoViewQuestion(bool aValue)
{
	 tiRemember->Enabled = aValue;
	 laRemember->Visible = aValue;
	 reRemember->Visible = aValue;
	 pbRemember->Visible = aValue;
	 laAnswer->Visible = ! aValue;
	 edAnswer->Visible = ! aValue;
	 buAnswer->Visible = ! aValue;
	 if (edAnswer->Visible) {
		 edAnswer->SetFocus();
	 }
}
//---------------------------------------------------------------------------
void __fastcall TFm::tiRememberTimer(TObject *Sender)
{
	pbRemember->Value +=1;
	if (pbRemember->Value >= pbRemember->Max) {
		DoViewQuestion(false);
	}
}
//---------------------------------------------------------------------------
void __fastcall TFm::FormCreate(TObject *Sender)
{
	Randomize();
    DoReset();
}
void __fastcall TFm::buResetClick(TObject *Sender)
{
    DoReset();
}
//---------------------------------------------------------------------------

void __fastcall TFm::buAnswerClick(TObject *Sender)
{
    DoAnswer();
}
//---------------------------------------------------------------------------


void __fastcall TFm::buZoomOutClick(TObject *Sender)
{
	ly->Scale->X -= 0.1;
	ly->Scale->Y -= 0.1;
	ly->RecalcSize();
}
//---------------------------------------------------------------------------

void __fastcall TFm::buZoomInClick(TObject *Sender)
{
	ly->Scale->X += 0.1;
	ly->Scale->Y += 0.1;
	ly->RecalcSize();
}
//---------------------------------------------------------------------------

void __fastcall TFm::edAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if (Key == vkReturn) {
		DoAnswer();
	}
}
//---------------------------------------------------------------------------



void __fastcall TFm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"����������� 2000 ��� ������ ������� �����. �������� ������� ������ 141-322, �������� ������");
}
//---------------------------------------------------------------------------

void __fastcall TFm::buStartClick(TObject *Sender)
{
	Sender->Free();
	tiRemember->Enabled = true;
}
//---------------------------------------------------------------------------

