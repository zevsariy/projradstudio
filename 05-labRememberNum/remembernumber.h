//---------------------------------------------------------------------------

#ifndef remembernumberH
#define remembernumberH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <System.Math.hpp>
//---------------------------------------------------------------------------
class TFm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buReset;
	TButton *buAbout;
	TLayout *ly;
	TRectangle *Rectangle1;
	TLabel *laCorrent;
	TRectangle *Rectangle2;
	TLabel *laWrong;
	TLabel *laRemember;
	TRectangle *reRemember;
	TLabel *laNumber;
	TProgressBar *pbRemember;
	TLabel *laAnswer;
	TEdit *edAnswer;
	TButton *buAnswer;
	TButton *buZoomOut;
	TButton *buZoomIn;
	TTimer *tiRemember;
	TStyleBook *StyleBook1;
	TButton *buStart;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall tiRememberTimer(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
	void __fastcall buAnswerClick(TObject *Sender);
	void __fastcall buZoomOutClick(TObject *Sender);
	void __fastcall buZoomInClick(TObject *Sender);
	void __fastcall edAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
private:	// User declarations
	int FCountCorrent;
	int FCountWrong;
	int FSecretNumber;
	void DoReset();
	void DoContinue();
	void DoAnswer();
	void DoViewQuestion(bool aValue);
public:		// User declarations
	__fastcall TFm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFm *Fm;
//---------------------------------------------------------------------------
#endif
