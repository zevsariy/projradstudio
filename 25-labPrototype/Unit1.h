//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.TabControl.hpp>
#include <System.ImageList.hpp>
#include <Data.Bind.GenData.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Fmx.Bind.GenData.hpp>
#include <FMX.ExtCtrls.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <IPPeerClient.hpp>
#include <REST.Client.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc1;
	TTabItem *ti1;
	TTabItem *ti2;
	TTabItem *ti3;
	TTabItem *ti4;
	TImageList *IL1;
	TLabel *la1;
	TTrackBar *TrackBar1;
	TGlyph *gl1;
	TBindingsList *BindingsList1;
	TLinkControlToProperty *LinkControlToPropertyText;
	TLinkControlToProperty *LinkControlToPropertyImageIndex;
	TListView *ListView1;
	TImageViewer *ImageViewer1;
	TEdit *Edit1;
	TEdit *Edit2;
	TPrototypeBindSource *PrototypeBindSource1;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkListControlToField *LinkListControlToField1;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
	TButton *Button1;
	TLinkPropertyToField *LinkPropertyToFieldEnabled;
	TRESTClient *RESTClient1;
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
