//---------------------------------------------------------------------------

#ifndef colorformH
#define colorformH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <System.UIConsts.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buRandom;
	TButton *buLightgreen;
	TLabel *laValue;
	TLabel *laRGB;
	void __fastcall buRandomClick(TObject *Sender);
	void __fastcall buLightgreenClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
    void NewColor(TAlphaColor aValue);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
