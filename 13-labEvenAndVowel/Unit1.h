//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiPlay;
	TTabItem *tiFinish;
	TTabItem *tiHelp;
	TLayout *la;
	TLabel *laTitle;
	TLabel *laPrava;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buStart;
	TButton *buHelp;
	TButton *buExit;
	TToolBar *ToolBar1;
	TButton *buReturnPlay;
	TLabel *laTitlePlay;
	TLabel *laScore;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buYes;
	TButton *buNo;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *Label1;
	TRectangle *re1;
	TLabel *Label2;
	TRectangle *re2;
	TLabel *lare2;
	TLabel *lare1;
	TTimer *tmPlay;
	TLayout *Layout2;
	TButton *buRestart;
	TLabel *laBad;
	TLabel *laGood;
	TToolBar *ToolBar2;
	TButton *buReturnFinish;
	TLabel *Label3;
	TLayout *Layout3;
	TImage *Image1;
	TToolBar *ToolBar3;
	TButton *buReturnHelp;
	TLabel *laHelp;
	TLabel *laTimer;
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall buHelpClick(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buReturnFinishClick(TObject *Sender);
	void __fastcall buReturnPlayClick(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
	void __fastcall buYesClick(TObject *Sender);
	void __fastcall buNoClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);

private:	// User declarations
	TDateTime FTime;
	int timerka;
	int CorrectCounter;
	int WrongCounter;
	int lare_set;
    int lare_num;
	bool My_Answer;
	bool Real_Answer;
	void Get_Task();
	void Read_Answer();
	int Score;
	void Restart_Game();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);

};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
