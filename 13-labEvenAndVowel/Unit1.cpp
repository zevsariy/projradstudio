//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;


void TForm1::Get_Task()
{
	lare_set = rand() % 2;
	if(lare_set ==0)
	{
		lare_num =  rand() % 10;
		switch ( lare_num )
		  {
			 case 1:
				lare1->Text = L"1A";
				lare2->Text = "";
				Real_Answer = false;
				break;
			 case 2:
				lare1->Text = L"5�";
				lare2->Text = "";
				Real_Answer = false;
				break;
			 case 3:
				lare1->Text = L"6�";
				lare2->Text = "";
				Real_Answer = true;
				break;
			 case 4:
				lare1->Text = L"9�";
				lare2->Text = "";
				Real_Answer = false;
				break;
			 case 5:
				lare1->Text = L"2�";
				lare2->Text = "";
				Real_Answer = true;
				break;
			 case 6:
				lare1->Text = L"6�";
				lare2->Text = "";
				Real_Answer = true;
				break;
			 case 7:
				lare1->Text = L"9�";
				lare2->Text = "";
				Real_Answer = false;
				break;
			 case 8:
				lare1->Text = L"3�";
				lare2->Text = "";
				Real_Answer = false;
				break;
			 case 9:
				lare1->Text = L"3�";
				lare2->Text = "";
				Real_Answer = false;
				break;
			 case 0:
				lare1->Text = L"1A";
				lare2->Text = "";
				Real_Answer = false;
				break;
		  }
	}
	else
	{
		switch ( lare_num )
		  {
			 case 1:
				lare1->Text = "";
				lare2->Text = L"2�";
				Real_Answer = true;
				break;
			 case 2:
				lare1->Text = "";
				lare2->Text = L"5�";
				Real_Answer = false;
				break;
			 case 3:
				lare1->Text = "";
				lare2->Text = L"3�";
				Real_Answer = true;
				break;
			 case 4:
				lare1->Text = "";
				lare2->Text = L"2�";
				Real_Answer = true;
				break;
			 case 5:
				lare1->Text = "";
				lare2->Text = L"9�";
				Real_Answer = false;
				break;
			 case 6:
				lare1->Text = L"";
				lare2->Text = L"4�";
				Real_Answer = false;
				break;
			 case 7:
				lare1->Text = L"";
				lare2->Text = L"9�";
				Real_Answer = false;
				break;
			 case 8:
				lare1->Text = L"";
				lare2->Text = L"7�";
				Real_Answer = true;
				break;
			 case 9:
				lare1->Text = L"";
				lare2->Text = L"9�";
				Real_Answer = true;
				break;
			 case 0:
				lare1->Text = "";
				lare2->Text = "7�";
				Real_Answer = false;
				break;
		  }
	}
}

void TForm1::Read_Answer()
{
   if(Real_Answer == My_Answer)
   {
	   CorrectCounter++;
	   Score++;
   }
   else
   {
	   WrongCounter++;
   }
   laScore->Text = Format(L"���� = %d", ARRAYOFCONST((Score)));
   Get_Task();
}

void TForm1::Restart_Game()
{
	Score = 0;
	CorrectCounter = 0;
	WrongCounter = 0;
	timerka = 30;
	laScore->Text = Format(L"���� = %d", ARRAYOFCONST((Score)));
}
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buExitClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buHelpClick(TObject *Sender)
{
	tc->ActiveTab = tiHelp;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStartClick(TObject *Sender)
{
	Get_Task();
	tc->ActiveTab = tiPlay;
	Restart_Game();
	tmPlay ->Enabled = True;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buReturnFinishClick(TObject *Sender)
{
	 tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buReturnPlayClick(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
	tmPlay->Enabled = false;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::tmPlayTimer(TObject *Sender)
{
	timerka--;
	laTimer->Text = timerka;
	if(timerka == 0)
	{
		tmPlay->Enabled = false;
		laGood->Text = Format(L"���������� = %d", ARRAYOFCONST((CorrectCounter)));
		laBad->Text = Format(L"������������ = %d", ARRAYOFCONST((WrongCounter)));
		tc->ActiveTab = tiFinish;
	}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::buYesClick(TObject *Sender)
{
	My_Answer = true;
	Read_Answer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buNoClick(TObject *Sender)
{
	My_Answer = false;
	Read_Answer();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::buRestartClick(TObject *Sender)
{
	Restart_Game();
	Get_Task();
	tc->ActiveTab = tiPlay;
	tmPlay ->Enabled = True;
}
//---------------------------------------------------------------------------

