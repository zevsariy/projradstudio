//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFm *Fm;
	void TFm::DoReset()
	{
		FCountCorrect = 0;
		FCountWrong = 0;
        DoContinue();
	}

	void TFm::DoContinue()
	{
		laCorrect->Text = Format(L"����� = %d", ARRAYOFCONST((FCountCorrect)));
		laWrong->Text = Format(L"�� ����� = %d", ARRAYOFCONST((FCountWrong)));
		int xValue1 = Random(20);
		int xValue2 = Random(20);
		int xSign = (Random(2) == 1) ? 1 : -1;
		int xResult = xValue1 + xValue2;
		int xResultNew = (Random(2) == 1) ? xResult: xResult + (Random(7) + xSign);

		FAnswerCorrect = (xResult == xResultNew);
		laCode ->Text = Format("%d + %d = %d", ARRAYOFCONST((xValue1, xValue2, xResultNew)));
	}

	void TFm::DoAnswer(bool aValue)
	{
		(aValue == FAnswerCorrect) ? FCountCorrect++ : FCountWrong++;
        DoContinue();
    }
//---------------------------------------------------------------------------
__fastcall TFm::TFm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TFm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"���������� �������� ������� �����, ������������� ������� ��������");
}
//---------------------------------------------------------------------------

void __fastcall TFm::FormCreate(TObject *Sender)
{
	Randomize();
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall TFm::buResetClick(TObject *Sender)
{
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall TFm::buYesClick(TObject *Sender)
{
	DoAnswer(true);
}
//---------------------------------------------------------------------------

void __fastcall TFm::buNoClick(TObject *Sender)
{
	DoAnswer(false);
}
//---------------------------------------------------------------------------

