//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbTop;
	TToolBar *tbOptions;
	TButton *buNew;
	TButton *buClear;
	TButton *buAbout;
	TButton *buBack;
	TButton *buDown;
	TButton *buUp;
	TButton *buForward;
	TButton *buDel;
	TTrackBar *tbRotation;
	TLayout *ly;
	TStyleBook *StyleBook1;
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buNewClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lyMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buBackMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buForwardClick(TObject *Sender);
	void __fastcall buUpClick(TObject *Sender);
	void __fastcall buDownClick(TObject *Sender);
	void __fastcall tbRotationChange(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
	TSelection *FSel;
    void ReSetSelection(TObject *Sender);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
