//---------------------------------------------------------------------------

#ifndef Lab1H
#define Lab1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *Clicker_Plus;
	TLabel *Rezult;
	TButton *Reset;
	TButton *Clicker_minus;
	TImageControl *ImageControl1;
	TButton *About;
	void __fastcall Clicker_PlusClick(TObject *Sender);
	void __fastcall ResetClick(TObject *Sender);
	void __fastcall Clicker_minusClick(TObject *Sender);
	void __fastcall AboutClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
