//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	if(pse->Enabled)
	{
		pse->Enabled = False;
	}
	else
	{
		pse->Enabled = True;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	if(RotateCrumpleTransitionEffect1->Enabled)
	{
		RotateCrumpleTransitionEffect1->Enabled = False;
	}
	else
	{
		RotateCrumpleTransitionEffect1->Enabled = True;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
if(InnerGlowEffect1->Enabled)
	{
		InnerGlowEffect1->Enabled = False;
	}
	else
	{
		InnerGlowEffect1->Enabled = True;
	}

}
//---------------------------------------------------------------------------

