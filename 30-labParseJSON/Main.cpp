//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm1 *fm1;
//---------------------------------------------------------------------------
__fastcall Tfm1::Tfm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::����������Click(TObject *Sender)
{
   me2->Lines->Clear();
   TJSONObject *xObj;
   TJSONArray *xArr;
   xObj = (TJSONObject*)TJSONObject::ParseJSONValue(me1->Text);

   xObj = (TJSONObject*)TJSONObject::ParseJSONValue(xObj->GetValue("response")->ToString());
   me2->Lines->Add("count: " + xObj->GetValue("count")->Value());

   xArr = (TJSONArray*) TJSONObject::ParseJSONValue(xObj->GetValue("items")->ToString());

   for(int i=0; i < xArr->Count; i++)
   {
		xObj = (TJSONObject*)TJSONObject::ParseJSONValue(xArr->Items[i]->ToString());
		me2->Lines->Add("id: " + xObj->GetValue("id")->Value());
		me2->Lines->Add("title: " + xObj->GetValue("title")->Value());
   }

   xArr->DisposeOf();
   xObj->DisposeOf();
}
//---------------------------------------------------------------------------
